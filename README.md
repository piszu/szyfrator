# SZYFRATOR - aplikacja szyfrująca #

## Version ##
### 2.0 (6) ###
* dodanie podziękowań

### 1.5 (5) - 1 maj 2015, 10:00:34 ###
* dodanie przycisku "Skopiuj wynik"
* dodanie przycisku z informacją o szyfrze podstawieniowym
* "Szyfruj/Deszyfruj" chowa klawiaturę

### 1.3 (4) ###
* niepoprawna publikacja w GooglePlay

### 1.1 (3) - 27 kwi 2015, 09:48:05  ###
* dodanie możliwości usuwania polskich znaków podczas szyfrowania
* poprawa layoutu

### 1.0.1 (2) - 24 kwi 2015, 00:07:28 ###
* Brak zmian, sprawdzenie jak zadziała aktualizacja automatyczna

### 1.0 (1) - 23 kwi 2015, 15:46:54 ###
* Pierwsza stabilna wersja aplikacji.

## To Do ##

* Writing tests
*
