package pl.mariuszpisz.szyfrator.utils;

import java.text.Normalizer;

public class ChangePolishSigns {

		public String Change(String ps_to_change){
			ps_to_change = Normalizer.normalize(ps_to_change, Normalizer.Form.NFD);
			ps_to_change = ps_to_change.replace("�", "l").replace("�", "L");
			ps_to_change = ps_to_change.replaceAll("[^��\\p{ASCII}]", "");
			return ps_to_change;
			
		}
}
