package pl.mariuszpisz.szyfrator.encrypt;

import java.util.HashMap;
import java.util.Objects;

import android.app.AlertDialog;
import pl.mariuszpisz.szyfrator.utils.ChangePolishSigns;

public class EncryptCezar {

	public String Encrypt(String ps_to_convert, int pn_alphabet,
			int pn_letters) {
		int ln_next_position;

		String ls_letter = new String("");
		String ls_return = new String("");
		String ls_current_letter = new String("");
		String ls_alphabet = new String("");
		
		//get alphabet
		if(pn_alphabet == 0){
			ls_alphabet = " A�BC�DE�FGHIJKL�MNO�PRS�TUWXYZ��";
		} else {
			ls_alphabet = " ABCDEFGHIJKLMNOPRSTUWXYZ";
		}
				
		int ln_alpha_len = ls_alphabet.trim().length();
		// check change position
		if (ln_alpha_len < pn_letters){
			return "ERROR";
		}
		

		// iterate for ps_to_convert
		for (int i = 0, n = ps_to_convert.length(); i < n; i++) {
			// get current letter
			ls_letter = Character.toString(ps_to_convert.charAt(i));

			// get letter position in alphabet
			int ln_position = ls_alphabet.indexOf(ls_letter.toUpperCase());

			// check is letter on alphabet
			if (ln_position > 0) {
				// get next position
				ln_next_position = ln_position + pn_letters;
				// check next position
				if (ln_next_position > ln_alpha_len) {
					ln_next_position = ln_next_position - ln_alpha_len;
				} else if (ln_next_position < 0) {
					ln_next_position = ln_alpha_len + ln_next_position;
				}
				// get letter from next position
				ls_current_letter = Character.toString(ls_alphabet
						.charAt(ln_next_position));
				// check upper and lowercase
				if (ls_letter != ls_letter.toUpperCase()) {
					ls_current_letter = ls_current_letter.toLowerCase();
				}
			} else {
				ls_current_letter = ls_letter;
			}
			ls_return = ls_return + ls_current_letter;
		}

		return ls_return;
	}
}
