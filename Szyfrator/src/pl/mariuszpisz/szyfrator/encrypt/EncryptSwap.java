package pl.mariuszpisz.szyfrator.encrypt;

import java.util.HashMap;

public class EncryptSwap {
	/*
	String GADERYPOLUKI = new String("GADERYPOLUKIgaderypoluki");
	String POLITYKARENU = new String("POLITYKARENUpolitykarenu");
	String KONIECMARUTY = new String("KONIECMATURYkoniecmatury");
	String NOWEBUTYLISA = new String("NOWEBUTYLISAnowebutylisa");
	String TOSIDEMARU = new String("TOSIDEMARUtosidemaru");
	String KULOPERYZAGI = new String("KULOPERYZAGIkuloperyzagi");
	String PADYGIMOZETU = new String("PADYGIMOZETUpadygimozetu");
	String KACEMINUTOWY = new String("KACEMINUTOWYkaceminutowy");
    */
	
	public String Encrypt(String ps_to_convert, String ps_crypto_list) {
		String afterConvert = new String("");
		
		HashMap<String, String> hm = new HashMap<String, String>();
		hm.put("GA-DE-RY-PO-LU-KI", new String("GADERYPOLUKIgaderypoluki") );
		hm.put("PO-LI-TY-KA-RE-NU", new String("POLITYKARENUpolitykarenu") );
		hm.put("KO-NI-EC-MA-TU-RY", new String("KONIECMATURYkoniecmatury") );
		hm.put("NO-WE-BU-TY-LI-SA", new String("NOWEBUTYLISAnowebutylisa") );
		hm.put("TO-SI-DE-MA-RU", new String("TOSIDEMARUtosidemaru") );
		hm.put("KU-LO-PE-RY-ZA-GI", new String("KULOPERYZAGIkuloperyzagi") );
		hm.put("PA-DY-GI-MO-ZE-TU", new String("PADYGIMOZETUpadygimozetu") );
		hm.put("KA-CE-MI-NU-TO-WY", new String("KACEMINUTOWYkaceminutowy") );
		hm.put("MO-TY-LE-CU-DA-KI", new String("MOTYLECUDAKImotylecudaki") );
		hm.put("MA-LI-NO-WE-BU-TY", new String("MALINOWEBUTYmalinowebuty") );
		hm.put("KA-TO-WI-CE-PU-LS", new String("KATOWICEPULSkatowicepuls") );
		
		
		char[] chars = ps_to_convert.toCharArray();
		char[] crypto = hm.get(ps_crypto_list).toCharArray();

		for (int i = 0, n = chars.length; i < n; i++) {
			boolean lb_not_match = true;
			for (int j = 0, m = crypto.length; j < m; j++) {

				if (chars[i] == crypto[j]) {
					lb_not_match = false;
					if (j % 2 == 0) {
						afterConvert = afterConvert + crypto[j + 1];
						break;
					} else {
						afterConvert = afterConvert + crypto[j - 1];
						break;
					}
				}
			}
			if (lb_not_match) {
				afterConvert = afterConvert + chars[i];
			}
		}

		return afterConvert;
	}

}
