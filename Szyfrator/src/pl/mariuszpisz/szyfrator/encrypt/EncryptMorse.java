package pl.mariuszpisz.szyfrator.encrypt;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Objects;

import android.app.AlertDialog;
import pl.mariuszpisz.szyfrator.utils.ChangePolishSigns;

public class EncryptMorse {

	/**
	 * Hash map for Morse Code
	 */
	private final static HashMap<String, String> GetMap() {
		final HashMap<String, String> morseCode = new HashMap<String, String>();
		morseCode.put("A", new String("� �"));
		morseCode.put("B", new String("� � � �"));
		morseCode.put("C", new String("� � � �"));
		morseCode.put("D", new String("� � �"));
		morseCode.put("E", new String("�"));
		morseCode.put("F", new String("� � � �"));
		morseCode.put("G", new String("� � �"));
		morseCode.put("H", new String("� � � �"));
		morseCode.put("I", new String("� �"));
		morseCode.put("J", new String("� � � �"));
		morseCode.put("K", new String("� � �"));
		morseCode.put("L", new String("� � � �"));
		morseCode.put("M", new String("� �"));
		morseCode.put("N", new String("� �"));
		morseCode.put("O", new String("� � �"));
		morseCode.put("P", new String("� � � �"));
		morseCode.put("Q", new String("� � � �"));
		morseCode.put("R", new String("� � �"));
		morseCode.put("S", new String("� � �"));
		morseCode.put("T", new String("�"));
		morseCode.put("U", new String("� � �"));
		morseCode.put("V", new String("� � � �"));
		morseCode.put("W", new String("� � �"));
		morseCode.put("X", new String("� � � �"));
		morseCode.put("Y", new String("� � � �"));
		morseCode.put("Z", new String("� � � �"));
		morseCode.put("1", new String("� � � � �"));
		morseCode.put("2", new String("� � � � �"));
		morseCode.put("3", new String("� � � � �"));
		morseCode.put("4", new String("� � � � �"));
		morseCode.put("5", new String("� � � � �"));
		morseCode.put("6", new String("� � � � �"));
		morseCode.put("7", new String("� � � � �"));
		morseCode.put("8", new String("� � � � �"));
		morseCode.put("9", new String("� � � � �"));
		morseCode.put("0", new String("� � � � �"));

		return morseCode;
	}

	private String getKeyByValue(String value) {
		for (Entry<String, String> entry : GetMap().entrySet()) {
			if (value.equals(entry.getValue())) {
				return entry.getKey().toString();
			}
		}
		return "";
	}

	private String prepare(String ps_to_prepare) {
		// removespaces
		ps_to_prepare = ps_to_prepare.replace(" ", "");
		// .
		ps_to_prepare = ps_to_prepare.replace(".", "�");
		// *
		ps_to_prepare = ps_to_prepare.replace("*", "�");
		// -
		ps_to_prepare = ps_to_prepare.replace("-", "�");
		// add spaces
		ps_to_prepare = ps_to_prepare.replace("�", " �");
		ps_to_prepare = ps_to_prepare.replace("�", " �");
		ps_to_prepare = ps_to_prepare.replace(" �/", " � /");
		ps_to_prepare = ps_to_prepare.replace(" �/", " � /");
		// trim
		ps_to_prepare = ps_to_prepare.trim();

		return ps_to_prepare;
	}

	public String Encrypt(String ps_to_convert) {
		String ls_return = new String();
		String ls_space = new String(" / ");
		String ls_helpner = new String();
		String ls_helpner_two = new String();
		ChangePolishSigns loc_changePolishSign = new ChangePolishSigns();
		
		// when null or empty move out
		if (ps_to_convert== null || ps_to_convert == "" ){
			return ps_to_convert;
		}
		
		
		// uppercase
		ps_to_convert = ps_to_convert.toUpperCase();
		// remowe PolishSign
		ps_to_convert = loc_changePolishSign.Change(ps_to_convert);

		char[] chars = ps_to_convert.toCharArray();
		for (int i = 0, n = chars.length; i < n; i++) {
			ls_helpner = "" + chars[i];
			
			// remove
			if (GetMap().get(ls_helpner) != null) {
				ls_return = ls_return + GetMap().get(ls_helpner) + ls_space;
			} else {
				// if first is space
				if (i >= 1) {
					// if this is space, and this-1 was letter
					ls_helpner_two = "" + chars[i - 1];
					if (ls_helpner.matches("^\\s*$")
							&& ls_helpner_two.matches("[a-zA-z��]{1}") // its a
																		// letter
							&& !ls_helpner_two.matches("[��]{1}")) // but its
																	// not
																	// ourcrypt
					{
						ls_return = ls_return.trim() + "/ ";
					} else {
						ls_return = ls_return + ls_helpner;
					}
				} else {
					ls_return = ls_return + ls_helpner;
				}
			}
		}

		// trim
		ls_return = ls_return.trim();
		// when end of transmision we add / if last sing is /
		if (ls_return.length() > 1) {
			if (ls_return.substring(ls_return.length() - 2).equals(" /")) {
				ls_return = ls_return + "/";
			}
		}
		
		return ls_return;
	}

	/**
	 * 
	 * @param ps_to_convert
	 * @return
	 */
	public String Decrypt(String ps_to_convert) {
		String ls_return = new String();
		String ls_helpner = new String();

		// prepare string
		ps_to_convert = prepare(ps_to_convert);

		String words[] = ps_to_convert.split("/");
/*
 * 
 */

	
/*
 * 		
 */
		for (int i = 0; i < words.length; i++) {
			ls_helpner = getKeyByValue(words[i].trim()).toString();
			// if we have helpner we add
			if (ls_helpner != "") {
				ls_return = ls_return + ls_helpner;
				// else if word is null, we have double // so we need space
			} else if (words[i].trim().equals("")) {
				ls_return = ls_return + " ";
			} else {
				ls_return = ls_return + words[i].trim();
			}
		}
		return ls_return.trim();
	}
}
