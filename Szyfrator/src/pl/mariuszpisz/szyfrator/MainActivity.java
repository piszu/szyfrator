package pl.mariuszpisz.szyfrator;

import java.io.ObjectOutputStream.PutField;

import pl.mariuszpisz.szyfrator.encrypt.EncryptBack;
import pl.mariuszpisz.szyfrator.encrypt.EncryptCezar;
import pl.mariuszpisz.szyfrator.encrypt.EncryptMorse;
import pl.mariuszpisz.szyfrator.encrypt.EncryptSwap;
import pl.mariuszpisz.szyfrator.utils.ChangePolishSigns;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	private CharSequence mDrawerTitle;
	private CharSequence mTitle;
	private String[] mPlanetTitles;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		// drawer
		mTitle = mDrawerTitle = getTitle();
		mPlanetTitles = getResources().getStringArray(R.array.nav_drawer_items);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.left_drawer);

		// set a custom shadow that overlays the main content when the drawer
		// opens
		mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
				GravityCompat.START);
		// set up the drawer's list view with items and click listener
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, mPlanetTitles));
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

		// enable ActionBar app icon to behave as action to toggle nav drawer
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		getActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#388E3C")));
		
		// ActionBarDrawerToggle ties together the the proper interactions
		// between the sliding drawer and the action bar app icon
		mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
		mDrawerLayout, /* DrawerLayout object */
		R.drawable.ic_drawer, /* nav drawer image to replace 'Up' caret */
		R.string.drawer_open, /* "open drawer" description for accessibility */
		R.string.drawer_close /* "close drawer" description for accessibility */
		) {

			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				invalidateOptionsMenu(); // creates call to
											// onPrepareOptionsMenu()
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			selectItem(0);
		}
	}

	/*
	 * DRAWER
	 */

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// MenuInflater inflater = getMenuInflater();
		// inflater.inflate(R.menu.main, menu);
		return super.onCreateOptionsMenu(menu);

	}

	/* Called whenever we call invalidateOptionsMenu() */
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content
		// view
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		// menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// The action bar home/up action should open or close the drawer.
		// ActionBarDrawerToggle will take care of this.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		// Handle action buttons
		/*
		 * switch(item.getItemId()) { case R.id.action_websearch: // create
		 * intent to perform web search for this planet Intent intent = new
		 * Intent(Intent.ACTION_WEB_SEARCH);
		 * intent.putExtra(SearchManager.QUERY, getActionBar().getTitle()); //
		 * catch event that there's no activity to handle intent if
		 * (intent.resolveActivity(getPackageManager()) != null) {
		 * startActivity(intent); } else { Toast.makeText(this,
		 * "R.string.app_not_available", Toast.LENGTH_LONG).show(); } return
		 * true; default: return super.onOptionsItemSelected(item); }
		 */
		return super.onOptionsItemSelected(item);
	}

	/* The click listner for ListView in the navigation drawer */
	private class DrawerItemClickListener implements
			ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			selectItem(position);
			hideKeyboard();
		}
	}

	private void selectItem(int position) {
		Fragment fragment;
		if (position == 0) {
			fragment = new HomeFragment();
		} else if (position == 1) {
			fragment = new SwapFragment();
		} else if (position == 2) {
			fragment = new MorseFragment();
		} else if (position == 3) {
			fragment = new BackFragment();
		} else if (position == 4) {
			fragment = new CezarFragment();
		} else if (position == 5) {
			fragment = new InfoFragment();
		} else {
			fragment = new HomeFragment();
		}
		FragmentManager fragmentManager = getFragmentManager();

		// update the main content by replacing fragments
		getFragmentManager().beginTransaction()
				.replace(R.id.content_frame, fragment).commit();

		// update selected item and title, then close the drawer
		mDrawerList.setItemChecked(position, true);
		setTitle(mPlanetTitles[position]);
		mDrawerLayout.closeDrawer(mDrawerList);
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/*
	 * END DRAWER
	 */

	/*
	 * MENU
	 */

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
	 * menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.main, menu); return true; }
	 * 
	 * @Override public boolean onOptionsItemSelected(MenuItem item) { // Handle
	 * action bar item clicks here. The action bar will // automatically handle
	 * clicks on the Home/Up button, so long // as you specify a parent activity
	 * in AndroidManifest.xml. int id = item.getItemId(); if (id ==
	 * R.id.action_info) { Intent intent = new Intent(this, InfoActivity.class);
	 * // start the second Activity this.startActivity(intent); return true; }
	 * return super.onOptionsItemSelected(item); }
	 */

	/*
	 * END MENU
	 */

	/*
	 * NOTHING SPECIAL?
	 * 
	 * @Override public void onClick(View view) { System.out.println("Dupa 0");
	 * if (view.getId() == R.id.action_info) { // define a new Intent for the
	 * second Activity } }
	 */

	/*
	 * UNIVERSAL
	 */

	/**
	 * hide Keyboard
	 */
	public void hideKeyboard() {
		// hide key
		InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
		// imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
		imm.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);

	}

	/**
	 * Get text from returText EditText
	 * 
	 * @return String
	 */
	public String getResultText() {
		EditText edit = (EditText) findViewById(R.id.returnText);
		return edit.getText().toString();
	}

	/**
	 * void setCopyResult Copy given text to Clopboard
	 * 
	 * @param ps_copy
	 */
	public void setCopyResult(String ps_copy) {
		ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
		ClipData clip = ClipData.newPlainText("Return Text", ps_copy);
		clipboard.setPrimaryClip(clip);
		Context context = getApplicationContext();
		CharSequence text = "Tekst zosta� skopiowany do schowka!";
		int duration = Toast.LENGTH_SHORT;

		Toast toast = Toast.makeText(context, text, duration);
		toast.show();
	}

	/**
	 * void onClickCopyResult Copy result to Clipboard
	 */
	public void onClickCopyResult(View view) {
		String ls_to_copy = getResultText();
		if (ls_to_copy != null && !ls_to_copy.isEmpty()) {
			setCopyResult(ls_to_copy);
		}
	}

	/*
	 * END UNIVERSAL
	 */

	/*
	 * SWAP ACTIVITY
	 */

	/**
	 * SwapEndcypt method
	 * 
	 * @param view
	 */
	public void onSwapClickEncrypt(View view) {
		// create String to convert
		String ls_to_convert = new String("");
		// call util classes
		EncryptSwap loc_EncryptSwap = new EncryptSwap();
		ChangePolishSigns loc_changePolishSign = new ChangePolishSigns();
		// get text from User

		EditText edit = (EditText) findViewById(R.id.mainText);
		ls_to_convert = edit.getText().toString();

		final CheckBox checkBox = (CheckBox) findViewById(R.id.checkPolishSign);
		if (checkBox.isChecked()) {
			ls_to_convert = loc_changePolishSign.Change(edit.getText()
					.toString());
		}

		// get text from spinner
		Spinner spinner = (Spinner) findViewById(R.id.spinner1);

		// show
		TextView t = new TextView(this);
		t = (TextView) findViewById(R.id.returnText);
		t.setText(loc_EncryptSwap.Encrypt(ls_to_convert, spinner
				.getSelectedItem().toString()));
		t.setTextIsSelectable(true);

		// hide key
		hideKeyboard();
	}

	/**
	 * void onSwapClickDialog Show dialogbox with information about Swap crypto
	 * on SwapFragment
	 */
	public void onSwapClickDialog(View arg0) {
		Context context = MainActivity.this;
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.modal_swap_info);
		//dialog.setTitle("Szyfry podstawieniowe");


		Button dialogButton = (Button) dialog
				.findViewById(R.id.ModalButtonClose);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();

		// fix for horizontal dialog
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = dialog.getWindow();
		lp.copyFrom(window.getAttributes());
		// This makes the dialog take up the full width
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		// end fix
	};

	/*
	 * END SWAP ACTIVITY
	 */

	/*
	 * MORSE ACTIVITY
	 */

	public void onMorseClickEncrypt(View view) {
		// create String to convert
		String ls_to_convert = new String("");
		EncryptMorse loc_EncrypMorse = new EncryptMorse();

		EditText edit = (EditText) findViewById(R.id.mainText);
		ls_to_convert = edit.getText().toString();

		// show
		TextView t = new TextView(this);
		t = (TextView) findViewById(R.id.returnText);
		t.setText(loc_EncrypMorse.Encrypt(ls_to_convert));
		t.setTextIsSelectable(true);

		// hide key
		hideKeyboard();
	}

	public void onMorseClickDecrypt(View view) {
		// create String to convert
		String ls_to_convert = new String("");
		EncryptMorse loc_EncrypMorse = new EncryptMorse();

		EditText edit = (EditText) findViewById(R.id.mainText);
		ls_to_convert = edit.getText().toString();

		// show
		TextView t = new TextView(this);
		t = (TextView) findViewById(R.id.returnText);

		t.setText(loc_EncrypMorse.Decrypt(ls_to_convert));
		t.setTextIsSelectable(true);

		// hide key
		hideKeyboard();
	}

	public void onMorseClickDialog(View arg0) {
		Context context = MainActivity.this;
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.modal_morse_info);
		dialog.setTitle("Kod Morse'a");
		Button dialogButton = (Button) dialog
				.findViewById(R.id.ModalButtonClose);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();

		// fix for horizontal dialog
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = dialog.getWindow();
		lp.copyFrom(window.getAttributes());
		// This makes the dialog take up the full width
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		// end fix
	};

	/*
	 * END MORSE ACTIVITY
	 */

	/*
	 * BACK ACTIVITY
	 */

	public void onBackClickEncrypt(View view) {
		// create String to convert
		String ls_to_convert = new String("");
		EncryptBack loc_EncrypBack = new EncryptBack();

		EditText edit = (EditText) findViewById(R.id.mainText);
		ls_to_convert = edit.getText().toString();

		// show
		TextView t = new TextView(this);
		t = (TextView) findViewById(R.id.returnText);

		t.setText(loc_EncrypBack.Encrypt(ls_to_convert));
		t.setTextIsSelectable(true);

		// hide key
		hideKeyboard();
	}

	/*
	 * END BACK ACTIVITY
	 */

	/*
	 * CEZAR ACTIVITY
	 */
	public void onCezarClickEncrypt(View view) {
		String ls_to_convert = new String("");
		String ls_letters = new String("");
		String ls_return = new String("");

		// call util classes
		EncryptCezar loc_EncryptCezar = new EncryptCezar();
		// get text from User

		EditText edit = (EditText) findViewById(R.id.mainText);
		ls_to_convert = edit.getText().toString();		
		// get text from spinner
		Spinner spinner = (Spinner) findViewById(R.id.spinner1);
		// get numbe of letters
		EditText editLetters = (EditText) findViewById(R.id.numberShiftField);
		ls_letters = editLetters.getText().toString();
		// null pointer exception
		if(ls_letters.length() == 0){
			ls_letters ="0";
		}
		// show
		TextView t = new TextView(this);
		t = (TextView) findViewById(R.id.returnText);
		ls_return = loc_EncryptCezar.Encrypt(ls_to_convert, spinner.getSelectedItemPosition(), Integer.parseInt(ls_letters));

		if (ls_return == "ERROR") {
		     Toast.makeText(getApplicationContext(), 
                     "Wprowadzone przesuni�cie o "+ls_letters+" znak�w(ki) jest zbyt d�ugie!", Toast.LENGTH_LONG).show();
		} else {
			t.setText(ls_return);
		}

		t.setTextIsSelectable(true);

		// hide key
		hideKeyboard();

	}


	public void onCezarClickDialog(View arg0) {
		Context context = MainActivity.this;
		// custom dialog
		final Dialog dialog = new Dialog(context);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		dialog.setContentView(R.layout.modal_cezar_info);
		dialog.setTitle("Szyfr Cezara");
		Button dialogButton = (Button) dialog
				.findViewById(R.id.ModalButtonClose);
		// if button is clicked, close the custom dialog
		dialogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});

		dialog.show();

		// fix for horizontal dialog
		WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
		Window window = dialog.getWindow();
		lp.copyFrom(window.getAttributes());
		// This makes the dialog take up the full width
		lp.width = WindowManager.LayoutParams.MATCH_PARENT;
		lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
		window.setAttributes(lp);
		// end fix
	};

	/*
	 * END CEZAR ACTIVITY
	 */

}
